module d1dev {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.web;
    requires jdk.jsobject;
    requires io.netty.all;

    opens d1dev.jrviewerx to javafx.fxml;
    exports d1dev.jrviewerx;

    opens d1dev.probe;
    exports d1dev.probe;
}