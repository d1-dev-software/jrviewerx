package d1dev.jrviewerx;

import d1dev.jrbustcp.JrbustcpClient;
import javafx.stage.Window;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Environment {
    private static Environment environment = null;

    public static final String CONF_FILENAME = "jrviwerx.conf";

    private final Path confPath = Paths.get("").resolve(CONF_FILENAME);
    private boolean confChanged = false;

    private JrbustcpClient jrclient = null;


    private static class ConfLine {
        String key;
        String value;
        boolean comment;

        public ConfLine(String key, String value, boolean comment) {
            this.key = key;
            this.value = value;
            this.comment = comment;
        }
    }
    private final Map<String, ConfLine> conf = new LinkedHashMap<>();

    private List<AutoCloseable> closeables = new ArrayList<>();

    public static Environment env() {
        if( environment == null )
            environment = new Environment();
        return environment;
    }

    public JrbustcpClient getJrbustcpClient() {
        if( jrclient == null)
            jrclient = new JrbustcpClient();
        return jrclient;
    }

    public void start() {
        loadConf();
    }

    public void addCloseable(AutoCloseable closeable) {
        closeables.add(closeable);
    }

    public void stop() throws Exception {
        for (AutoCloseable closeable : closeables) {
            closeable.close();
        }
        saveConf();
    }

    private void addConfLine(String key, String value, boolean comment) {
        conf.put(key, new ConfLine(key, value, comment));
        confChanged = true;
    }

    private void removeConfLine(String key) {
        conf.remove(key);
        confChanged = true;
    }

    private void loadConf() {
        if( !Files.exists(confPath) )
            return;

        Pattern p = Pattern.compile("(\\S+)\\s*=\\s*(.*)");
        try {
            Files.lines(confPath, Charset.forName("UTF-8")).forEach(line -> {
                String s = line.trim();
                if( s.isEmpty()  ||  s.startsWith("#")) {
                    String key = "#" + conf.size();
                    addConfLine(key, line, true);
                } else {
                    var m = p.matcher(s);
                    if( m.matches() ) {
                        addConfLine(m.group(1), m.group(2), false);
                    }
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        confChanged = false;
    }


    private void saveConf() {
        if( !confChanged )
            return;

        List<String> lines = conf.values().stream()
                .map(line -> (line.comment ? "" : line.key + "=") + line.value)
                .collect(Collectors.toList());

        try {
            Files.write(confPath, lines);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    public boolean getProperty(String key, boolean value) {
        try {
            var line = conf.get(key);
            if( line != null  &&  !line.value.isEmpty() )
                value = line.value.trim().equals("1");
        } catch (NumberFormatException e) {
        }
        return value;
    }

    public int getProperty(String key, int value) {
        try {
            var line = conf.get(key);
            if( line != null  &&  !line.value.isEmpty() )
                value = Integer.parseInt(line.value);
        } catch (NumberFormatException e) {
        }
        return value;
    }

    public double getProperty(String key, double value) {
        try {
            var line = conf.get(key);
            if( line != null  &&  !line.value.isEmpty() )
                value = Double.parseDouble(line.value);
        } catch (NumberFormatException e) {
        }
        return value;
    }

    public String getProperty(String key, String value) {
        var line = conf.get(key);
        if( line != null )
            value = line.value;
        return value;
    }

    public void setProperty(String key, boolean value) {
        setProperty(key, value? "1": "0");
    }

    public void setProperty(String key, int value) {
        setProperty(key, "" + value);
    }

    public void setProperty(String key, double value) {
        setProperty(key, "" + value);
    }

    public void setProperty(String key, String value) {
        var line = conf.get(key);
        if( line != null ) {
            if( !line.value.equals(value) ) {
                line.value = value;
                confChanged = true;
            }
        } else {
            addConfLine(key, value, false);
        }
    }


    public void loadWindowConf(Window wnd, String name, double x, double y, double width, double height) {
        wnd.setX( getProperty(name + ".x", x) );
        wnd.setY( getProperty(name + ".y", y) );
        wnd.setWidth( getProperty(name + ".width", width) );
        wnd.setHeight( getProperty(name + ".height", height) );
    }


    public void saveWindowConf(Window wnd, String name) {
        setProperty(name + ".x", wnd.getX());
        setProperty(name + ".y", wnd.getY());
        setProperty(name + ".width", wnd.getWidth());
        setProperty(name + ".height", wnd.getHeight());
    }

    public List<String> getListProperty(String key) {
        key += ".";
        List<String> values = new ArrayList<>();
        for(int i=0; conf.containsKey(key+i); ++i)
            values.add( getProperty(key+i, "") );
        return values;
    }

    public void setListProperty(String key, List<String> values) {
        key += ".";
        for(int i=0; i<values.size(); ++i)
            setProperty(key+i, values.get(i));

        for(int i=values.size(); conf.containsKey(key+i); ++i)
            removeConfLine(key+i);
    }
}
