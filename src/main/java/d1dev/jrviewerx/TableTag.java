package d1dev.jrviewerx;

import d1dev.tags.Tag;
import d1dev.tags.TagPlain;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class TableTag {
    final Tag tagClient;
    final Tag tagValue;
    final StringProperty name;
    final StringProperty type;
    final StringProperty value;

    public TableTag(Object bean, Tag tag) {
        this.tagClient = tag;
        this.tagValue = TagPlain.create(tag);
        this.name = new SimpleStringProperty(bean, "name", tag.getName());
        this.type = new SimpleStringProperty(bean, "type", tag.getType().name());
        this.value = new SimpleStringProperty(bean, "value", tag.getString());

    }


    @Override
    public String toString() {
        return "TableTag{" +
                "name=" + name.getValue() +
                ", type=" + type.getValue() +
                ", value=" + value.getValue() +
                '}';
    }


    public void update() {
        if( !tagClient.equalsValue(tagValue) ) {
            tagClient.copyValueTo(tagValue);
            value.set(tagValue.getString());
        }
    }
}
