package d1dev.jrviewerx;

import d1dev.tags.Tag;
import javafx.beans.InvalidationListener;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.*;

import java.net.URL;
import java.util.Comparator;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import static d1dev.jrviewerx.Environment.env;


public class TableController implements Initializable, AutoCloseable {


    public TextField edFilter;
    public Button btFilters;
    public TableView<TableTag> table;

    private TableColumn<TableTag, String> colTagname;
    private TableColumn<TableTag, String> colType;
    private TableColumn<TableTag, String> colValue;

    private TableColumn<TableTag, ?> colSort;

    private InvalidationListener columnWidthListener;
    private ObservableList<TableTag> list = FXCollections.observableArrayList();


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        colTagname = new TableColumn<>("Tagname");
        colType = new TableColumn<>("Type");
        colValue = new TableColumn<>("Value");

        colTagname.setCellValueFactory(param -> param.getValue().name);
        colType.setCellValueFactory(param -> param.getValue().type);
        colValue.setCellValueFactory(param -> param.getValue().value);

        table.getColumns().clear();
        table.getColumns().add(colTagname);
        table.getColumns().add(colType);
        table.getColumns().add(colValue);


        table.getColumns().forEach(col -> {
            col.getStyleClass().add("table-column");
            col.setReorderable(false);
        });

        colValue.setEditable(true);
        colValue.setCellFactory(TextFieldTableCell.forTableColumn());
        colValue.setOnEditCommit(this::onValueEditCommit);

        columnWidthListener = TableViewResizer.init(table, () -> columnWidthListener, 0, 2, 2, 0);

        initTableSort();

        table.setItems( list );

        table.getSortOrder().add(colTagname);

        initTableMouseClick();

        initTableKeyPress();

        initTableEditableIfNotEmpty();

        initTableContextMenu();
    }

    private void initTableContextMenu() {
        MenuItem m1 = new MenuItem("Copy Selected Tagname");
        m1.setOnAction(this::copyClipboardSelectedTagname);
        m1.setAccelerator( KeyCombination.keyCombination("Ctrl+C"));

        MenuItem m2 = new MenuItem("Copy All");
        m2.setOnAction(this::copyClipboardAll);
        m2.setAccelerator( KeyCombination.keyCombination("Ctrl+S"));

        table.setContextMenu( new ContextMenu(m1, m2) );
    }


    private void copyClipboardAll(ActionEvent actionEvent) {
        int l1 = list.stream().mapToInt(tableTag -> tableTag.name.get().length()).max().orElse(0);
        int l2 = list.stream().mapToInt(tableTag -> tableTag.type.get().length()).max().orElse(0);
        int l3 = list.stream().mapToInt(tableTag -> tableTag.value.get().length()).max().orElse(0);
        var fmt = "%-" + l1 + "s   %-" + l2 + "s   %-" + l3 + "s\r\n";
        var sb = new StringBuilder();
        list.forEach(tableTag -> {
            sb.append(String.format(fmt, tableTag.name.get(), tableTag.type.get(), tableTag.value.get()));
        });
        copyClipboardString(sb.toString());
    }


    private void copyClipboardSelectedTagname(ActionEvent actionEvent) {
        var tableTag = table.getSelectionModel().getSelectedItem();
        if( tableTag != null ) {
            copyClipboardString(tableTag.name.get());
        }
    }


    private void copyClipboardString(String s) {
        ClipboardContent cc = new ClipboardContent();
        cc.putString(s);
        Clipboard.getSystemClipboard().setContent(cc);
    }


    private void initTableEditableIfNotEmpty() {
        table.editableProperty().bind( Bindings.createBooleanBinding(() -> list.size() > 0, list));
    }

    private void initTableKeyPress() {
        table.setOnKeyPressed(event -> {
            int idx = table.getSelectionModel().getFocusedIndex();
            if( event.getCode() == KeyCode.ENTER  &&  idx >= 0) {
                table.edit(idx, colValue);
            }
        });
    }

    private void initTableMouseClick() {
        table.setRowFactory( tv -> {
            TableRow<TableTag> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if( event.getClickCount() == 2  &&  !row.isEmpty() ) {
                    table.edit(row.getIndex(), colValue);
                }
            });
            return row ;
        });
    }


    private void initTableSort() {
        // disable multiple column sorting
        table.setOnSort(sortEvent -> {
            while (table.getSortOrder().size() > 1) {
                table.getSortOrder().remove(1);
            }
        });

        table.setSortPolicy(param -> {

            if( table.getSortOrder().size() == 0 ) {
                if( colSort == null )
                    colSort = colTagname;

                colSort.setSortType( colSort.getSortType() == TableColumn.SortType.ASCENDING?
                        TableColumn.SortType.DESCENDING:
                        TableColumn.SortType.ASCENDING);

                table.getSortOrder().add(colSort);
            }

            var col = table.getSortOrder().get(0);
            colSort = col;

            Comparator<TableTag> comparator = null;
            int dir = col.getSortType() == TableColumn.SortType.ASCENDING? 1: -1;

            if( col == colTagname ) {
                comparator = (o1, o2) -> dir * o1.tagClient.getName().compareTo( o2.tagClient.getName() );
            }

            if( col == colType ) {
                comparator = (o1, o2) -> {
                    int res = o1.tagClient.getType().name().compareTo( o2.tagClient.getType().name());
                    if( res == 0)
                        res = o1.tagClient.getName().compareTo( o2.tagClient.getName());
                    return dir * res;
                };
            }

            if( col == colValue ) {
                comparator = (o1, o2) -> {
                    var tag1 = o1.tagClient;
                    var tag2 = o2.tagClient;
                    var tt1 = tag1.getType();
                    var tt2 = tag2.getType();

                    int res;
                    if( tt1 == Tag.Type.BOOL  &&  tt2 != Tag.Type.BOOL ) {
                        res = -1;
                    } else if( tt1 != Tag.Type.BOOL  &&  tt2 == Tag.Type.BOOL ) {
                        res = 1;
                    } else if( tt1 == Tag.Type.STRING  &&  tt2 != Tag.Type.STRING ) {
                        res = 1;
                    } else if( tt1 != Tag.Type.STRING  &&  tt2 == Tag.Type.STRING ) {
                        res = -1;
                    } else if( tt1 == Tag.Type.STRING  &&  tt2 == Tag.Type.STRING ) {
                        res = 0;
                    } else if( tt1 == Tag.Type.DOUBLE  ||  tt2 == Tag.Type.DOUBLE ) {
                        double d = tag1.getDouble() - tag2.getDouble();
                        res = d == 0? 0: d < 0? -1: 1;
                    } else if( tt1 == Tag.Type.LONG  ||  tt2 == Tag.Type.LONG ) {
                        long l = tag1.getLong() - tag2.getLong();
                        res = l == 0? 0: l < 0? -1: 1;
                    } else {
                        res = tag1.getInt() - tag2.getInt();
                    }

                    if( res == 0 )
                        res = tag1.getName().compareTo(tag2.getName());

                    return dir * res;
                };
            }

            if( comparator != null ) {
                FXCollections.sort(list, comparator);
            }

            return true;
        });
    }


    @Override
    public void close() throws Exception {
    }


    public String getColumnWidthsAsString() {
        return table.getColumns().stream()
                .map(col -> "" + col.getWidth())
                .collect(Collectors.joining(";"));
    }


    public void setColumnWidthsFromString(String colWidths) {
        var ws = colWidths.split(";");
        if( ws.length == 3 ) {
            try {
                for (int i = 0; i < 3; ++i) {
                    table.getColumns().get(i).setPrefWidth(Double.parseDouble(ws[i]));
                }
            } catch (NumberFormatException e) {
            }
        }
    }


    public void updateListContent() {
        var tagmap = env().getJrbustcpClient().getTagtable().getWildcardMap( edFilter.getText() );
        list.removeIf(tableTag -> !tagmap.containsKey(tableTag.tagClient.getName()));

        var tagset = list.stream()
                .map(tableTag -> tableTag.tagClient.getName())
                .collect(Collectors.toSet());

        tagmap.values().stream().forEach(tag -> {
            if( !tagset.contains(tag.getName()) )
                list.add( new TableTag(this, tag) );
        });

        table.sort();
    }


    public void updateValues() {
        list.forEach(TableTag::update);
    }


    public void onFilterKeyPressed(KeyEvent keyEvent) {
        if( keyEvent.getCode() == KeyCode.ENTER ) {
            updateListContent();
        }
    }


    private void onValueEditCommit(TableColumn.CellEditEvent<TableTag, String> event) {
        TableTag tableTag = event.getRowValue();
        String value = event.getNewValue();

        tableTag.tagValue.setString( value );
        tableTag.value.set( value );
        tableTag.value.set( tableTag.tagValue.getString() );
        tableTag.tagClient.setString( value );

        table.requestFocus();
    }


    public void onBtFilters(ActionEvent actionEvent) {
        table.scrollTo( table.getSelectionModel().getFocusedIndex() );
    }
}
