package d1dev.jrviewerx;

import javafx.beans.InvalidationListener;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumnBase;
import javafx.scene.control.TableView;

import java.util.function.Supplier;

public class TableViewResizer {

    public static InvalidationListener init(
            TableView<?> table, Supplier<InvalidationListener> getListener, int idxColAuto, int... resizePolicy) {

        table.setColumnResizePolicy(TableView.UNCONSTRAINED_RESIZE_POLICY);

        InvalidationListener listener = observable -> {
            table.getColumns().forEach(col1 -> col1.widthProperty().removeListener(getListener.get()));

            var col = (TableColumn<?,?>)(((ReadOnlyDoubleProperty)observable).getBean());
            int i = table.getColumns().indexOf(col);
            if( i >= 0  &&  i < table.getColumns().size()) {
                adjust(table, resizePolicy[i]);
            }
            table.getColumns().forEach(col2 -> col2.widthProperty().addListener(getListener.get()));
        };

        table.getColumns().forEach(col2 -> col2.widthProperty().addListener(listener));
        table.widthProperty().addListener(o -> adjust(table, idxColAuto));

        return listener;
    }

    private static void adjust(TableView<?> table, int idx) {
        double widthTable = table.getWidth() - 19;
        double widthSum = table.getColumns().stream()
                .mapToDouble(TableColumnBase::getWidth)
                .sum();

        double widthCol = widthTable - widthSum + table.getColumns().get(idx).getWidth();
        table.getColumns().get(idx).setPrefWidth(widthCol);
    }

}
