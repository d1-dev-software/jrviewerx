package d1dev.jrviewerx;

import javafx.beans.value.ChangeListener;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;

import java.io.IOException;

public class MainTab extends Tab {
    public static final String FXML_TABLE = "/d1dev/jrviewerx/fxml/table.fxml";

    private TableController controller;
    private Label label;

    public TableController getController() {
        return controller;
    }

    public Label getHeaderLabel() {
        return label;
    }

    public MainTab(String name, String filter, String colWidths) {
        label = new Label(name);
        setGraphic(label);

        FXMLLoader loader = new FXMLLoader( getClass().getResource(FXML_TABLE));
        try {
            Parent p = loader.load();
            controller = loader.getController();
            setContent(p);
            controller.edFilter.setText(filter);
            controller.setColumnWidthsFromString(colWidths);
        } catch (IOException e) {
            e.printStackTrace();
        }

        label.setOnMousePressed(mouseEvent -> {
            if( mouseEvent.getButton().equals(MouseButton.PRIMARY)  &&  mouseEvent.getClickCount() == 2){
                mouseEvent.consume();

                TextField tf = new TextField(label.getText());
                String cancelText = label.getText();
                label.setText("");
                label.setGraphic(tf);
                tf.requestFocus();
                tf.selectAll();

                ChangeListener<Boolean> cancelListener = (observable, oldValue, newValue) -> {
                    if (!newValue) {
                        label.setText(cancelText);
                        label.setGraphic(null);
                    }
                };
                tf.focusedProperty().addListener(cancelListener);

                tf.setOnKeyPressed(keyEvent -> {
                    if( keyEvent.getCode() == KeyCode.ENTER ) {
                        label.setText( tf.getText() );
                        label.setGraphic(null);
                        tf.focusedProperty().removeListener(cancelListener);
                    }
                    if( keyEvent.getCode() == KeyCode.ESCAPE ) {
                        label.setText( cancelText );
                        label.setGraphic(null);
                    }
                });
            }
        });

    }
}
