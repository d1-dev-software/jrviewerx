package d1dev.jrviewerx;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import static d1dev.jrviewerx.Environment.env;

public class JrviewerxApp extends Application {

    public static final String APP_TITLE = "JrViewerX";
    private Stage stage;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        this.stage = stage;

        env().start();
        env().loadWindowConf(stage, "window", 100, 100, 640, 480);

        FXMLLoader loader = new FXMLLoader( getClass().getResource("/d1dev/jrviewerx/fxml/main.fxml"));
        Scene scene = new Scene(loader.load(), stage.getWidth(), stage.getHeight());

        stage.setTitle(APP_TITLE);
        stage.setMinWidth(300);
        stage.setMinHeight(300);
        stage.setScene(scene);
        stage.show();

    }

    @Override
    public void stop() throws Exception {
        env().saveWindowConf(stage, "window");

        env().stop();
        super.stop();
    }
}
