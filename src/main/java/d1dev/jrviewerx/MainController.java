package d1dev.jrviewerx;

import d1dev.jrbustcp.JrbustcpClient;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.When;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Stream;

import static d1dev.jrviewerx.Environment.env;

public class MainController implements Initializable, AutoCloseable {
//    public static final String FXML_TABLE = "/d1dev/jrviewerx/fxml/table.fxml";

    public SplitPane paneMain;
    public AnchorPane paneLeft;
    public AnchorPane paneRight;
    public TabPane tabPane;
    public Button btAdd;
    public Button btConnect;
    public TextField edHost;
    public TextField edPort;
    public TextField edPeriod;
    public TextField edClientDescr;
    public TextField edRemoteFilter;
    public CheckBox cbExternal;
    public CheckBox cbHidden;
    public Label lbTagCounter;
    public Label lbTime;
    public Button btAddFilter;
    public Button btRemoveFilter;
    public ListView<String> listViewFilters;

    private TableController selectedTableController;

    ObservableList<String> listFilters = FXCollections.observableArrayList();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        env().addCloseable(this);


        tabPane.getSelectionModel().selectedItemProperty().addListener(o -> onTabSelected());

        tabPane.tabClosingPolicyProperty().bind(Bindings.createObjectBinding(() ->
                        tabPane.getTabs().size() > 1?
                                TabPane.TabClosingPolicy.SELECTED_TAB:
                                TabPane.TabClosingPolicy.UNAVAILABLE,
                tabPane.getTabs() ));

        tabPane.setTabDragPolicy(TabPane.TabDragPolicy.REORDER);

        var cl = env().getJrbustcpClient();
        cl.stateProperty().addListener((observable, oldValue, newValue) -> updateControls(newValue));
        cl.setOnListChanged(this::updateList);
        cl.setOnValuesChanged(this::updateValues);

        lbTime.textProperty().bind(
                new When( cl.stateProperty().isEqualTo(JrbustcpClient.State.RUNNING) )
                        .then(cl.timeProperty().asString())
                        .otherwise("")
        );

        load();

        initListViewFilters();
    }



    @Override
    public void close() throws Exception {
        System.out.println("Controller close");
        save();
    }

    private Stream<MainTab> getTabStream() {
        return tabPane.getTabs().stream().map(tab -> (MainTab) tab);
    }



    private void load() {
        edHost.setText( env().getProperty("host", "localhost"));
        edPort.setText( env().getProperty("port", "40000"));
        edPeriod.setText( env().getProperty("period", "500"));
        edClientDescr.setText( env().getProperty("client.descr", "jrviewerx"));
        edRemoteFilter.setText( env().getProperty("remote.filter", ""));
        cbExternal.setSelected( env().getProperty("external", false) );
        cbHidden.setSelected( env().getProperty("hidden", false) );

        paneMain.setDividerPositions( env().getProperty("window.split", 0.3));

        var tabNames = env().getListProperty("tab.name");
        var tabFilters = env().getListProperty("tab.filter");
        var tabColWidths = env().getListProperty("tab.colwidths");

        tabPane.getTabs().clear();

        int n = Math.min(tabNames.size(), tabFilters.size());
        n = Math.min(n, tabColWidths.size());

        if( n == 0) {
            onAddTab(null);
        } else {
            for(int i=0; i<n; ++i) {
                createTab(tabNames.get(i), tabFilters.get(i), tabColWidths.get(i));
            }
        }
        tabPane.getSelectionModel().select( 0);

        listFilters.addAll( env().getListProperty("localfilter") );
    }


    private void save() {
        env().setProperty("host", edHost.getText() );
        env().setProperty("port", edPort.getText() );
        env().setProperty("period", edPeriod.getText() );
        env().setProperty("client.descr", edClientDescr.getText() );
        env().setProperty("remote.filter", edRemoteFilter.getText() );
        env().setProperty("external", cbExternal.isSelected() );
        env().setProperty("hidden", cbHidden.isSelected() );

        env().setProperty("window.split", paneMain.getDividerPositions()[0]);

        var tabNames = new ArrayList<String>();
        var tabFilters = new ArrayList<String>();
        var tabColWidths = new ArrayList<String>();

        getTabStream().forEach(tab -> {
            tabNames.add( tab.getHeaderLabel().getText()  );
            var controller = tab.getController();
            tabFilters.add( controller.edFilter.getText() );
            tabColWidths.add( controller.getColumnWidthsAsString() );
        });

        env().setListProperty("tab.name", tabNames);
        env().setListProperty("tab.filter", tabFilters);
        env().setListProperty("tab.colwidths", tabColWidths);

        env().setListProperty("localfilter", listFilters);
    }



    public void onAddTab(Event event) {
        createTab("Tags", "", "0;100;200");
    }


    private void createTab(String name, String filter, String colWidths) {
        MainTab tab = new MainTab(name, filter, colWidths);
        tabPane.getTabs().add(tab);
        tabPane.getSelectionModel().select(tab);
        tab.getController().updateListContent();
    }


    public void onBtConnect(ActionEvent actionEvent) {
        var cl = env().getJrbustcpClient();
        if( cl.getState() == JrbustcpClient.State.OFF ) {
            setupJrclient();
            cl.start();
        } else {
            cl.stop();
        }
    }


    private void updateControls(JrbustcpClient.State state) {
        System.out.println("updateControls: " + state);

        paneLeft.lookupAll(".connect-parameter")
                .forEach(node -> node.setDisable(state != JrbustcpClient.State.OFF));

        btConnect.setDisable(
                state == JrbustcpClient.State.CONNECTING  ||
                state == JrbustcpClient.State.DISCONNECTING);

        var style = btConnect.getStyleClass();

        if( state == JrbustcpClient.State.OFF ) {
            style.removeIf(s -> s.startsWith("btconnect"));
            style.add("btconnect-disconnected");
            btConnect.setText("Connect");
        }

        if( state == JrbustcpClient.State.CONNECTING ) {
            btConnect.setText("Connecting...");
        }

        if( state == JrbustcpClient.State.RUNNING ) {
            style.removeIf(s -> s.startsWith("btconnect"));
            style.add("btconnect-connected");
            btConnect.setText("Disconnect");
        }

        if( state == JrbustcpClient.State.DISCONNECTING ) {
            btConnect.setText("Disconnecting...");
        }

        if( state == JrbustcpClient.State.ERROR ) {
            System.out.println("JrbustcpClient.State.ERROR");
        }

    }


    private void setupJrclient() {
        var cl = env().getJrbustcpClient();

        if( edHost.getText().trim().isEmpty() )
            edHost.setText("localhost");
        cl.setHost( edHost.getText() );

        int port = 40000;
        try {
            port = Integer.parseInt(edPort.getText());
        } catch (NumberFormatException e) {
        }
        cl.setPort( port );

        int period = 500;
        try {
            period = Integer.parseInt(edPeriod.getText());
        } catch (NumberFormatException e) {
        }
        cl.setPeriod( period );

        cl.setDescr( edClientDescr.getText() );

        cl.setFilter( edRemoteFilter.getText() );

        cl.setFlagExcludeExternal( cbExternal.isSelected() );

        cl.setFlagIncludeHidden( cbHidden.isSelected() );

    }


    private void updateList() {
        lbTagCounter.setText( "" + env().getJrbustcpClient().getTagtable().getSize() );

        getTabStream().forEach(tab -> tab.getController().updateListContent());
    }


    private void updateValues() {
        if( selectedTableController != null)
            selectedTableController.updateValues();
    }


    private void onTabSelected() {
        var tab = (MainTab)tabPane.getSelectionModel().getSelectedItem();
        if( tab == null ) {
            selectedTableController = null;
        } else {
            selectedTableController = tab.getController();
            updateValues();
        }
    }


    private void initListViewFilters() {
        listViewFilters.setItems( listFilters );

        listViewFilters.setOnMouseClicked(event -> {
            if( event.getClickCount() == 2 )
                applySelectedFilter();
        });

        listViewFilters.setOnKeyPressed(event -> {
            if( event.getCode() == KeyCode.ENTER ) {
                applySelectedFilter();
            }
        });

    }


    private void applySelectedFilter() {
        var value = listViewFilters.getSelectionModel().getSelectedItem();
        if( value != null ) {
            selectedTableController.edFilter.setText( value );
            selectedTableController.updateListContent();
        }
    }


    public void onBtAddFilter(ActionEvent actionEvent) {
        var value = selectedTableController.edFilter.getText();
        if( value.trim().isEmpty()  ||  listFilters.contains(value) )
            return;
        listFilters.add( value );
        listFilters.sort(String::compareTo);
    }


    public void onBtRemoveFilter(ActionEvent actionEvent) {
        int idx = listViewFilters.getSelectionModel().getSelectedIndex();
        if( idx >= 0 )
            listViewFilters.getItems().remove(idx);
    }
}


