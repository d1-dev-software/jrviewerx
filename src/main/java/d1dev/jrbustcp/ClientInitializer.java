package d1dev.jrbustcp;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.ssl.SslContext;

import static d1dev.jrbustcp.JrbustcpProtocol.FRAME_SIZE_MAX;

public class ClientInitializer extends ChannelInitializer<SocketChannel> {
    private static final int TRAFFIC_CHECK_INTERVAL_S = 3;
    private final JrbustcpClient client;
    private final SslContext sslCtx;


    public ClientInitializer(JrbustcpClient client, SslContext sslCtx) {
        this.client = client;
        this.sslCtx = sslCtx;
    }

    @Override
    public void initChannel(SocketChannel ch) {
        ChannelPipeline pipeline = ch.pipeline();

        pipeline.addFirst("traffic", new TrafficHandler());

        if (sslCtx != null) {
            pipeline.addLast(sslCtx.newHandler(ch.alloc()));
        }

        pipeline.addLast(new LengthFieldBasedFrameDecoder(FRAME_SIZE_MAX, 0, 2, 0, 2));
        pipeline.addLast(new LengthFieldPrepender(2));

        pipeline.addLast(new ClientHandler(client));
    }

}
