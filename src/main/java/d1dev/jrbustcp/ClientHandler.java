package d1dev.jrbustcp;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

public class ClientHandler extends ChannelInboundHandlerAdapter {
    private JrbustcpClient client;

    public ClientHandler(JrbustcpClient client) {
        this.client = client;
    }

    private String getAddress(ChannelHandlerContext ctx) {
        return ctx.channel().localAddress().toString();
    }


    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        ByteBuf buf = (ByteBuf) msg;
        client.answer.offer(buf);
    }


    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        ctx.close();
    }



}
