package d1dev.jrbustcp;

import javafx.application.Platform;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.ssl.SslContext;
import d1dev.tags.TagTable;
import d1dev.tags.Tag;
import d1dev.tags.TagRW;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleObjectProperty;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.stream.Collectors;

import static d1dev.jrbustcp.JrbustcpProtocol.*;
import static d1dev.jrviewerx.Environment.env;


public class JrbustcpClient implements AutoCloseable {


    public enum State {
        OFF,
        CONNECTING,
        RUNNING,
        DISCONNECTING,
        ERROR
    }
    private final SimpleObjectProperty<State> state = new SimpleObjectProperty<>(this, "state", State.OFF);
    private final SimpleLongProperty time = new SimpleLongProperty(this, "passtime", 0);

    private Runnable onListChanged = null;
    private Runnable onValuesChanged = null;

    private String descr;
    private String host;
    private int port;
    private volatile int period;
    private boolean flagExcludeExternal;
    private boolean flagIncludeHidden;
    private String filter;
    private int timeout;
    private volatile String lastError;


    private Bootstrap bootstrap;
    private EventLoopGroup workerGroup = null;
    private Channel channel = null;
    private ByteBuf outbuf = null;
    private ByteBuf inbuf = null;
    private int reqid;
    private boolean hasReadValues;
    private boolean hasListChanges;

    private final JrbustcpProtocol prot = new JrbustcpProtocol();
    private final List<Integer> writeCache = new ArrayList<>();
    final BlockingQueue<ByteBuf> answer = new LinkedBlockingQueue<>(10);
    private TagRW[] tags = new TagRW[0];
    private final TagTable tagtable = new TagTable();

    private Thread thread;

    public JrbustcpClient() {
        host = "localhost";
        port = 40000;
        period = 500;
        flagExcludeExternal = false;
        flagIncludeHidden = false;
        timeout = 3000;

        env().addCloseable(this);
    }




    public synchronized void start() {

        if( thread != null  &&  thread.isAlive() ) {
            return;
        }

        state.set( State.CONNECTING );
        thread = new Thread(() -> {

            hasListChanges = false;

            if( connect() ) {
                if( hasListChanges  &&  onListChanged != null ) {
                    Platform.runLater(() -> onListChanged.run());
                }

                Platform.runLater(() -> state.set(State.RUNNING));

                try {
                    long timebeg;
                    long timeend;
                    long t;
                    long timecycle = System.currentTimeMillis();
                    while (!Thread.interrupted()) {

                        hasListChanges = false;
                        hasReadValues = false;

                        timebeg = System.currentTimeMillis();
                        if (!execute()) {
                            Platform.runLater(() -> state.set(State.ERROR));
                            break;
                        }
                        timeend = System.currentTimeMillis();

                        final long time = timeend - timebeg;
                        Platform.runLater(() -> this.time.set(time));

                        if( hasListChanges  &&  onListChanged != null ) {
                            Platform.runLater(() -> onListChanged.run());
                        }

                        if( hasReadValues  &&  onValuesChanged != null ) {
                            Platform.runLater(() -> onValuesChanged.run());
                        }

                        long timesleep = period - (timeend - timecycle);
                        if (timesleep > 0) {
                            Thread.sleep(timesleep);
                            timecycle += period;
                        } else
                            timecycle = timeend;
                    }
                    System.out.println("run: after while");
                } catch (InterruptedException e) {
                    System.out.println("interrupted in run");
                }
            }
            Platform.runLater(() -> state.set(State.DISCONNECTING));
            disconnect();

            Platform.runLater(() -> state.set(State.OFF));
        });

        thread.start();

    }


    public synchronized void stop() {
        if( thread != null )
            thread.interrupt();
    }


    @Override
    public synchronized void close() {
        if( thread != null ) {
            try {
                thread.interrupt();
                thread.join();
            } catch (InterruptedException e) {
                System.out.println("interrupted in close 1");
            }
        }

        try {
            if( workerGroup != null )
                workerGroup.shutdownGracefully().sync();
        } catch (InterruptedException e) {
            System.out.println("interrupted in close 2");
        }

        workerGroup = null;
        bootstrap = null;
    }




    private boolean connect() {
        System.out.println("connect");

        boolean connected = false;
        reqid = ThreadLocalRandom.current().nextInt();

        if( workerGroup == null)
            workerGroup = new NioEventLoopGroup(1);

        if( bootstrap == null ) {
            SslContext sslCtx = null;

            bootstrap = new Bootstrap();
            bootstrap.group(workerGroup);
            bootstrap.channel(NioSocketChannel.class);
            bootstrap.option(ChannelOption.SO_KEEPALIVE, true);
            bootstrap.handler(new ClientInitializer(this, sslCtx));
        }

        try {
            channel = bootstrap.connect(host, port).sync().channel();
            if( initList()  &&  doRead() ) {
                for(TagRW tag: tags) {
                    tag.hasWriteValue();
                }
                connected = true;
            }
        } catch (Exception e) {
            setLastError(e);
        }

        return connected;
    }

    private void setLastError(Exception e) {
        lastError = LocalDateTime.now().toString() + ": " + e.getMessage();
    }


    private void disconnect() {
        System.out.println("disconnect");
        if( channel == null )
            return;

        for(TagRW tag: tags) {
            tag.setStatus(Tag.Status.Bad);
        }

        try {
            if(outbuf != null) {
                outbuf.release();
                outbuf = null;
            }
            channel.close().sync();
        } catch (Exception e) {
        }

        channel = null;
    }


    protected boolean execute() throws InterruptedException {
        try {
            if( isConnected()  &&  doWrite()  &&  doRead() )
                return true;
        } catch (InterruptedException e) {
            throw e;
        } catch (Exception e) {

        } finally {
            releaseInbuf();
        }

        return false;
    }


    protected boolean isConnected() {
        return channel != null  &&  channel.isActive();
    }


    private Message readMessage(int cmd) throws TimeoutException, InterruptedException {
        for(;;) {
            releaseInbuf();
            inbuf = answer.poll(timeout, TimeUnit.MILLISECONDS);

            if (inbuf == null)
                throw new TimeoutException();

            Message msg = prot.getMessage(inbuf);
            if (msg.reqId == reqid) {
                if( msg.cmd != (cmd | 0x80))
                    throw new IllegalStateException("Bad command: " + msg.cmd);
                return msg;
            }
        }
    }

    private void releaseInbuf() {
        if( inbuf != null ) {
            inbuf.release();
            inbuf = null;
        }
    }

    private void createOutbuf() {
        if( outbuf == null ) {
            outbuf = channel.alloc().buffer();
        }
    }

    private void writeAndFlash() {
        channel.writeAndFlush(outbuf);
        outbuf = null;

    }

    private int getNextReqId() {
        return ++reqid;
    }


    protected boolean initList() throws InterruptedException {
        TagRW[] newtags = null;
        try {
            Message msg = sendCmdInit();

            int size = msg.body.readUnsignedMedium();
            newtags = new TagRW[size];

            int next = 0;
            do {
                msg = sendCmdList(next);
                int index = msg.body.readUnsignedMedium();
                int qnt = msg.body.readUnsignedMedium();
                next = msg.body.readUnsignedMedium();

                for(int i=0; i<qnt; ++i) {
                    Tag.Type type = prot.convertByteToTagType(msg.body.readUnsignedByte());

                    int len = msg.body.readUnsignedByte();
                    String tagname = msg.body.readCharSequence(len, charset).toString();

                    len = msg.body.readUnsignedByte();
                    String tagdescr = msg.body.readCharSequence(len, charset).toString();

                    if( index < 0  ||  index >= size )
                        throw new IndexOutOfBoundsException();

                    newtags[index] = TagRW.create(type, tagname, 0);
                    newtags[index].setStatus( Tag.Status.Uninitiated);
                    index++;
                }
            } while (next > 0);

        } catch (InterruptedException e) {
            throw e;
        } catch (Exception e) {
            setLastError(e);
            return false;
        } finally {
            releaseInbuf();
        }

        // remove irrelevant tags from old
        Map<String,Tag> newmap = Arrays.stream(newtags).collect(
                Collectors.toMap(Tag::getName, tag -> tag, (a1, a2) -> a1));

        boolean hasChanges = false;

        for(Tag oldtag: tags) {
            Tag newtag = newmap.get(oldtag.getName());
            if( newtag==null  ||  newtag.getType() != oldtag.getType() ) {
                tagtable.remove(oldtag);
                hasChanges = true;
            }
        }

        // move relevant from old to new
        for(int i=0; i<newtags.length; ++i) {
            Tag oldtag = tagtable.get(newtags[i].getName());

            if( oldtag == null ) {
                tagtable.add(newtags[i]);
                hasChanges = true;
            } else
                newtags[i] = (TagRW)oldtag;

            newtags[i].setStatus(Tag.Status.Good);
        }

        if( hasChanges ) {
            tags = newtags;
            hasListChanges = hasChanges;
        }

        return true;
    }


    private Message sendCmdInit() throws TimeoutException, InterruptedException {
        createOutbuf();
        prot.writeHeader(outbuf, getNextReqId(), CMD_INIT);
        prot.writeShortString(outbuf, filter);
        prot.writeShortString(outbuf, descr);
        outbuf.writeShort( getInitFlags() );
        prot.writeFooter(outbuf);
        writeAndFlash();
        return readMessage(CMD_INIT);
    }


    private int getInitFlags() {
        int initflags = 0; // no INITPRM_TAGDESCR;
        initflags += INITPRM_TAGSTATUS;
        initflags += (flagExcludeExternal?1:0) * INITPRM_EXCLUDE_EXTERNAL;
        initflags += (flagIncludeHidden?1:0) * INITPRM_INCLUDE_HIDDEN;
        return initflags;
    }


    private Message sendCmdList(int index) throws TimeoutException, InterruptedException {
        createOutbuf();
        prot.writeHeader(outbuf, getNextReqId(), CMD_LIST);
        outbuf.writeMedium( index );
        prot.writeFooter(outbuf);
        writeAndFlash();
        return readMessage(CMD_LIST);
    }


    protected boolean doWrite() throws InterruptedException {

        int qnt = 0;
        for(int i=0; i<tags.length; ++i) {
            if( tags[i].hasWriteValue() ) {
                if( qnt < writeCache.size() )
                    writeCache.set(qnt, i) ;
                else
                    writeCache.add(i);
                qnt++;
            }
        }

        if( qnt == 0 )
            return true;

        try {
            int i = 0;
            while( i < qnt ) {
                int curindex = writeCache.get(i);
                createOutbuf();
                prot.writeHeader(outbuf, getNextReqId(), CMD_WRITE);
                outbuf.writeMedium( curindex );
                int qntPos = outbuf.writerIndex();
                outbuf.writeMedium(0);

                int n = 0;
                for(;i<qnt; ++i, ++n, ++curindex) {
                    if( !prot.canWrite(outbuf) )
                        break;

                    int index = writeCache.get(i);
                    boolean gap = curindex != index;

                    if( gap ) {
                        prot.writeIndex(outbuf, index);
                        curindex = index;
                    }

                    prot.writeValue(outbuf, tags[curindex]);
                }

                outbuf.setMedium(qntPos, n);
                prot.writeFooter(outbuf);
                writeAndFlash();

                readMessage(CMD_WRITE);
            }
        } catch (InterruptedException e) {
            System.out.println("write: 1");
            throw e;
        } catch (Exception e) {
            System.out.println("write: 2");
//            env.logError(logger, e, name, "write error");
            setLastError(e);
            return false;
        } finally {
            releaseInbuf();
        }

        return true;
    }


    protected boolean doRead() throws InterruptedException {
        try {
            Message msg = sendCmdUpdate();
            int qnt = msg.body.readUnsignedMedium();
            int next = msg.body.readUnsignedMedium();
            int liststate = msg.body.readUnsignedByte();

            if( liststate == 0xFF ) {
                return initList();
            }

            if( qnt == 0 )
                return true;

            int index;
            do {
                msg = sendCmdRead(next);

                index = msg.body.readUnsignedMedium();
                qnt = msg.body.readUnsignedMedium();
                next = msg.body.readUnsignedMedium();

                for (int i = 0; i < qnt; ++i, ++index) {
                    index = prot.readIndex(msg.body, index);
                    if (index >= 0 && index < tags.length)
                        prot.readValueAndStatus(msg.body, tags[index]);
                    else
                        throw new IndexOutOfBoundsException();

                    hasReadValues = true;
                }
            } while (next > 0);

        } catch (InterruptedException e) {
            System.out.println("read: 1");
            throw e;
        } catch (Exception e) {
            System.out.println("read: 2");
            setLastError(e);
            return false;
        } finally {
            releaseInbuf();
        }

        return true;
    }

    private Message sendCmdUpdate() throws TimeoutException, InterruptedException {
        createOutbuf();
        prot.writeHeader(outbuf, getNextReqId(), CMD_UPDATE);
        prot.writeFooter(outbuf);
        writeAndFlash();
        return readMessage(CMD_UPDATE);
    }

    private Message sendCmdRead(int index) throws TimeoutException, InterruptedException {
        createOutbuf();
        prot.writeHeader(outbuf, getNextReqId(), CMD_READ);
        outbuf.writeMedium( index );
        prot.writeFooter(outbuf);
        writeAndFlash();
        return readMessage(CMD_READ);
    }


    public State getState() {
        return state.get();
    }

    public SimpleObjectProperty<State> stateProperty() {
        return state;
    }

    public long getTime() {
        return time.get();
    }

    public SimpleLongProperty timeProperty() {
        return time;
    }

    public void setOnListChanged(Runnable onListChanged) {
        this.onListChanged = onListChanged;
    }

    public void setOnValuesChanged(Runnable onValuesChanged) {
        this.onValuesChanged = onValuesChanged;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public boolean isFlagExcludeExternal() {
        return flagExcludeExternal;
    }

    public void setFlagExcludeExternal(boolean flagExcludeExternal) {
        this.flagExcludeExternal = flagExcludeExternal;
    }

    public boolean isFlagIncludeHidden() {
        return flagIncludeHidden;
    }

    public void setFlagIncludeHidden(boolean flagIncludeHidden) {
        this.flagIncludeHidden = flagIncludeHidden;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public TagTable getTagtable() {
        return tagtable;
    }
}
