package d1dev.jrbustcp;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class Message {
    private final static Charset charset = StandardCharsets.UTF_8;

    public int     cmd;
    public int     reqId;
    public int     size;
    public ByteBuf body;


    public Message(int command, int reqId, int size, ByteBuf body) {
        this.cmd = command;
        this.reqId = reqId;
        this.size = size;
        this.body = body;
    }

    @Override
    public String toString() {
        return "Message{" +
                "cmd=" + cmd +
                ", reqId=" + reqId +
                ", size=" + size +
                ", body=" + ByteBufUtil.hexDump(body) +
                '}';
    }

}
