package d1dev.probe;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import javafx.scene.Cursor;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class ParticleSystem extends GraphicApp {

	private List<Emitter> emitters = new ArrayList<>();
	private Emitter followEmitter = null;

	Random random = new Random();

	public static void main(String[] args) {
		launch();
	}

	@Override
	public void setup() {
		frames(50);
		width = 1200;
		height = 800;

		graphicContext.getCanvas().setOnMousePressed(e -> {
			createFollowEmitter(e.getSceneX(), e.getSceneY());
			followEmitter.pos(e.getSceneX(), e.getSceneY());
			graphicContext.getCanvas().setCursor(Cursor.DISAPPEAR);
		});

		graphicContext.getCanvas().setOnMouseDragged(e -> {
			if(followEmitter != null)
				followEmitter.move(e.getSceneX(), e.getSceneY());
		});

		graphicContext.getCanvas().setOnMouseReleased(event -> {
			removeFollowEmitter();
			graphicContext.getCanvas().setCursor(Cursor.DEFAULT);
		});

		title("Simple Particle System");
	}

	private void createFollowEmitter(double x, double y) {
		if( followEmitter == null ) {
			followEmitter = new Emitter(x, y);
			emitters.add( followEmitter );
		}
	}

	private void removeFollowEmitter() {
		if( followEmitter != null ) {
			followEmitter.active = false;
			followEmitter = null;
		}
	}

	@Override
	public void draw() {
		for (Emitter emitter : emitters) {
			emitter.emit(graphicContext);
		}
		emitters.removeIf(emitter -> emitter.dead);
	}

	public class Emitter {

		public boolean active = true;
		public boolean dead = false;

		List<Particle> particles = new ArrayList<>();
		double x, y;
		double moveX, moveY;

		public Emitter(double x, double y) {
			this.x = x;
			this.y = y;
		}

		public void emit(GraphicsContext gc) {
			if( active ) {
				double l = Math.sqrt((moveX - x)*(moveX - x) + (moveY - y)*(moveY - y)) + 1;
				double dx = (moveX - x) / l;
				double dy = (moveY - y) / l;
				for (int i = 0; i < l; i++) {
					x += dx;
					y += dy;
					int duration = random.nextInt(100) + 2;
					double yDir = (random.nextDouble() - 0.5) * 1;//0.75;
					double xDir = (random.nextDouble() - 0.5) * 1;//0.75;
					Particle p = new Particle(x, y, duration, xDir, yDir);
					particles.add(p);
				}
				x = moveX;
				y = moveY;
			}

			for (Particle particle : particles) {
				particle.step();
				particle.show(gc);
			}
			particles = particles.stream().filter(p -> p.timeleft > 0).collect(Collectors.toList());
			dead = !active  &&  particles.size() == 0;
		}

		public void pos(double x, double y) {
			this.x = x;
			this.y = y;
			move(x, y);
		}

		public void move(double x, double y) {
			this.moveX = x;
			this.moveY = y;
		}
	}

	public class Particle {
		private final int duration;
		int timeleft;
		double x, y, yDir, xDir, size;
		double op;

		public Particle(double x, double y, int duration, double yDir, double xDir) {
			this.x = x;
			this.y = y;
			this.timeleft = duration;
			this.duration = duration;
			this.yDir = yDir;
			this.xDir = xDir;
			this.size = random.nextDouble() * 4 + 1;
		}

		public void step() {
			x += xDir;
			y += yDir;
			timeleft--;
			op = ((double)timeleft / (double)duration) * 1;//0.6;
		}

		public void show(GraphicsContext gc) {
			gc.setFill(Color.rgb(255, 20, 20, op));
			gc.fillOval(x, y, size, size);
		}
	}

}

