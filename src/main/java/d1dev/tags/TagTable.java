package d1dev.tags;

import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import java.util.stream.Collectors;

public class TagTable {

	private final Map<String,Tag> tagmap;
	
	
	public TagTable() {
		this.tagmap = new HashMap<>();
	}

	public Tag get(String name) {
		return tagmap.get(name);
	}

	
	public Collection<Tag> values() {
		return tagmap.values();
	}

	public Map<String,Tag> getWildcardMap(String wildcards) {
		String regex = wildcards.replace("*", ".*").replace(';', '|');
		return getRegexMap(regex);
	}

	public Map<String,Tag> getRegexMap(String regex) {
		Pattern p1 = null;
		try {
			if( !regex.isEmpty() )
				p1 = Pattern.compile(regex);
		} catch (PatternSyntaxException e) {
		}
		final Pattern p = p1;

		if( p == null) {
			return getMap();
		}

		return tagmap.entrySet().stream()
				.filter(entry -> p.matcher(entry.getKey()).matches())
				.collect(Collectors.toUnmodifiableMap(Map.Entry::getKey, Map.Entry::getValue));
	}

	
	public Map<String,Tag> getMap() {
		return Collections.unmodifiableMap(tagmap);
	}


	public int getSize() {
		return tagmap.size();
	}


	public <T extends Tag> T add(T tag) {
		tagmap.put(tag.getName(), tag);
		return tag;
	}

	public boolean remove(Tag tag) {
		if (tag == null)
			return false;
		else
			tag.setStatus( Tag.Status.Deleted );
		
		return tagmap.remove(tag.getName()) == tag;
	}


	public void add(List<Tag> taglist) {
		taglist.forEach(this::add);
	}

	public void remove(List<Tag> taglist) {
		taglist.forEach(this::remove);
	}


	public void clear() {
		tagmap.entrySet().forEach(ent -> ent.getValue().setStatus(Tag.Status.Deleted));
		tagmap.clear();
	}


	public TagRW createTagRW(Tag.Type tagtype, String name) {
		TagRW tag = TagRW.create(tagtype, name, 0);
		return add( tag );
	}

	public TagRW createRWBool(String name, boolean value) {
		return add(new TagRWBool(name, value));
	}

	public TagRW createRWInt(String name, int value) {
		return add(new TagRWInt(name, value));
	}

	public TagRW createRWLong(String name, long value) {
		return add(new TagRWLong(name, value));
	}

	public TagRW createRWDouble(String name, double value) {
		return add(new TagRWDouble(name, value));
	}

	public TagRW createRWString(String name, String value) {
		return add(new TagRWString(name, value));
	}


}
