package d1dev.tags;


public abstract class TagBase implements Tag {

	private final String name;
	private volatile Status status = Status.Good;
	private Object object = null;


	public TagBase(String name) {
		this.name = name;
	}


	@Override
	public Status getStatus() {
		return status;
	}


	@Override
	public void setStatus(Status status) {
		if( this.status != status )
			this.status = status;
	}


	@Override
	public String getName() {
		return name;
	}


	@Override
	public String toString() {
		return getString();
	}


	@Override
	@SuppressWarnings("unchecked")
	public <T> T getObject() {
		return (T)object;
	}


	@Override
	public void setObject(Object object) {
		this.object = object;
	}

}
