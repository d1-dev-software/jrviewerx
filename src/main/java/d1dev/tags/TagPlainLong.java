package d1dev.tags;


public class TagPlainLong extends TagPlain {

	long value;


	public TagPlainLong(String name, long value) {
		super(name);
		this.value = value;
	}

	@Override
	public Type getType() {
		return Type.LONG;
	}

	@Override
	public boolean equalsValue(Tag tag) {
		return tag.getLong() == value;
	}

	@Override
	public void copyValueTo(Tag tag) {
		tag.setLong(value);
	}

	
	
	
	
	@Override
	public boolean getBool() {
		return value!=0;
	}

	@Override
	public int getInt() {
		return (int)value;
	}

	@Override
	public long getLong() {
		return value;
	}

	@Override
	public double getDouble() {
		return (double)value;
	}

	@Override
	public String getString() {
		return ""+value;
	}
	


	
	
	@Override
	public void setBool(boolean value) {
		this.value = value? 1: 0;
	}

	@Override
	public void setInt(int value) {
		this.value = value;
	}

	@Override
	public void setLong(long value) {
		this.value = value;
	}

	@Override
	public void setDouble(double value) {
		this.value = (long)value;
	}

	@Override
	public void setString(String value) {
		try {
			this.value = Long.parseLong(value.trim());
		} catch (NumberFormatException e) {
			this.value = 0L;
		}
	}

}
